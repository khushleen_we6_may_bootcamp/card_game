from random import choice

DIAMONDS = list(range(1, 14))
#FACECARDS = {1 : "A", 11 : "J", 12 : "Q", 13 : "K"}
FACECARDS = (("A",1),("J",11),("Q",12),("K",13))
def auction(DIAMONDS):
    auctioned_card = choice(DIAMONDS)
    DIAMONDS = [_ for _ in DIAMONDS if _ != auctioned_card]
    if auctioned_card in FACECARDS:
        return FACECARDS[auctioned_card]
    return auctioned_card

def face_to_numbers(card1: str):
    #return [value for (value, card) in FACECARDS.items() if card1 in FACECARDS.values()]
    #return item for item in FACECARDS if FACECARDS[item] == card1
    for a,b in FACECARDS:
        if a == card1:
            return b

#def compare(a_player, b_player,)
def compare_players(bid1: int, bid2: int, auction_card: int|str, count1: int = 0, count2: int = 0):
    string_list = ["A", "K","Q","J"]
    if auction_card in string_list:
        auction_value = face_to_numbers(auction_card)
    else: 
        auction_value = auction_card
    if bid1 > bid2:
        count1 += auction_value
    elif bid1 == bid2:
        count1 += auction_value / 2
        count2 += auction_value / 2
    else:
        count2 += auction_value

    return count1, count2

points1, points2 = 0,0
n = 0
while n != 13:
    auction_card = auction(DIAMONDS)
    print(auction_card)
    print("Enter bid for player 1")
    bid1 = input()
    print("Enter bid for player 2")
    bid2 = input()
    points_current1, points_current2 = compare_players(bid1,bid2,auction_card)
    points1 += points_current1
    points2 += points_current2
    print(points1,points2)
    n += 1

print(f'Player 1 total points are {points1}, Player 2 total points are {points2}')
if points1 > points2:
    print("Player 1 wins")
elif points1 == points2:
    print("Game draw")
else:
    print("Player 2 wins")
    



    